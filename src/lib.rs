/*
 *
 * Rust implementation of the Spectre algorithm (https://spectre.app/)
 *
 * By Théo Battrel <theo.battrel@protonmail.ch>
 * Started on july 2019
 *
 * This work is licensed under the MIT License.
 * To view a copy of this license, visit https://opensource.org/license/MIT/
 * or see LICENSE.
 *
 * For more informations about  the Masterpassword implementation take a look at
 * https://spectre.app/spectre-algorithm.pdf/
 *
 */

pub mod crypto;
pub mod key_scope;
pub mod mpversion;
pub mod mpw;
pub mod template_characters;
pub mod template_classe;
