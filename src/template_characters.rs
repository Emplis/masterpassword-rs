/**
 * `TemplateCharacters`
 */
#[allow(non_camel_case_types)]
#[derive(Clone, Copy)]
pub enum TemplateCharacters {
    V, // upper vowels
    C, // upper consonants
    v, // vowels
    c, // consonants
    A, // upper alphabetic
    a, // alphabetic
    n, // numeric
    o, // other
    x, // union set
    S, // Space
}

/**
 * Return a vector of the characters corresponding to the given characters template.
 *
 * Argument:
 *
 * - `character_class` - characters template.
 */
pub fn character_classes(character_class: &TemplateCharacters) -> Vec<char> {
    // ------- begin character_classes -------

    match character_class {
        TemplateCharacters::V => "AEIOU".chars().collect(),
        TemplateCharacters::C => "BCDFGHJKLMNPQRSTVWXYZ".chars().collect(),
        TemplateCharacters::v => "aeiou".chars().collect(),
        TemplateCharacters::c => "bcdfghjklmnpqrstvwxyz".chars().collect(),
        TemplateCharacters::A => "AEIOUBCDFGHJKLMNPQRSTVWXYZ".chars().collect(),
        TemplateCharacters::a => "AEIOUaeiouBCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz"
            .chars()
            .collect(),
        TemplateCharacters::n => "0123456789".chars().collect(),
        TemplateCharacters::o => "@&%?,=[]_:-+*$#!'^~;()/.".chars().collect(),
        TemplateCharacters::x => {
            "AEIOUaeiouBCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz0123456789!@#$%^&*()"
                .chars()
                .collect()
        }
        TemplateCharacters::S => " ".chars().collect(),
    }

    // ------- end character_classes -------
}
