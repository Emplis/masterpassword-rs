/**
 * `KeyScope`
 *
 * The Spectre algorithm define who are used to scope the generation of key
 * to a specific purpose.
 *
 * Three purpose are defined:
 *
 * - `Authentification`:
 * Used for generating a key that is used for the authentification of the user,
 * for example a password.
 *
 * - `Identification`:
 * Used for generating a key that is used for the identification of the user,
 * for example a login name.
 *
 * - `Recovery`:
 * Used for generating a key that us used for fall-back authentification of the user,
 * for example a security question answer.
 */
pub enum KeyScope {
    Authentification,
    Identification,
    Recovery,
}

/**
 * Return the key scope value.
 *
 * Argument:
 *
 * - `purpose` - key scope name, refer to the `KeyScope` definition for more informations.
 */
pub fn key_scope(purpose: KeyScope) -> &'static str {
    // ------- begin key_scope -------

    match purpose {
        KeyScope::Authentification => "com.lyndir.masterpassword",
        KeyScope::Identification => "com.lyndir.masterpassword.login",
        KeyScope::Recovery => "com.lyndir.masterpassword.answer",
    }

    // ------- end key_scope -------
}
