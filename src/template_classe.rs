use crate::template_characters::TemplateCharacters;
use TemplateCharacters::*;

/**
 * `TemplateClasse`
 */
#[derive(Clone, Copy)]
pub enum TemplateClasse {
    MaximumSecurity,
    Long,
    Medium,
    Short,
    Basic,
    PIN,
    Name,
    Phrase,
}

/**
 * Return all the characters templates for the given password template.
 *
 * Argument:
 *
 * - `template_class` - password template.
 */
pub fn template_classes(template_class: TemplateClasse) -> Vec<Vec<TemplateCharacters>> {
    // ------- begin template_classes -------

    match template_class {
        TemplateClasse::MaximumSecurity => vec![
            vec![a, n, o, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x], // anoxxxxxxxxxxxxxxxxx
            vec![a, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, n, o], // axxxxxxxxxxxxxxxxxno
        ],
        TemplateClasse::Long => vec![
            vec![C, v, c, v, n, o, C, v, c, v, C, v, c, v], // CvcvnoCvcvCvcv
            vec![C, v, c, v, C, v, c, v, n, o, C, v, c, v], // CvcvCvcvnoCvcv
            vec![C, v, c, v, C, v, c, v, C, v, c, v, n, o], // CvcvCvcvCvcvno
            vec![C, v, c, c, n, o, C, v, c, v, C, v, c, v], // CvccnoCvcvCvcv
            vec![C, v, c, c, C, v, c, v, n, o, C, v, c, v], // CvccCvcvnoCvcv
            vec![C, v, c, c, C, v, c, v, C, v, c, v, n, o], // CvccCvcvCvcvno
            vec![C, v, c, v, n, o, C, v, c, c, C, v, c, v], // CvcvnoCvccCvcv
            vec![C, v, c, v, C, v, c, c, n, o, C, v, c, v], // CvcvCvccnoCvcv
            vec![C, v, c, v, C, v, c, c, C, v, c, v, n, o], // CvcvCvccCvcvno
            vec![C, v, c, v, n, o, C, v, c, v, C, v, c, c], // CvcvnoCvcvCvcc
            vec![C, v, c, v, C, v, c, v, n, o, C, v, c, c], // CvcvCvcvnoCvcc
            vec![C, v, c, v, C, v, c, v, C, v, c, c, n, o], // CvcvCvcvCvccno
            vec![C, v, c, c, n, o, C, v, c, c, C, v, c, v], // CvccnoCvccCvcv
            vec![C, v, c, c, C, v, c, c, n, o, C, v, c, v], // CvccCvccnoCvcv
            vec![C, v, c, c, C, v, c, c, C, v, c, v, n, o], // CvccCvccCvcvno
            vec![C, v, c, v, n, o, C, v, c, c, C, v, c, c], // CvcvnoCvccCvcc
            vec![C, v, c, v, C, v, c, c, n, o, C, v, c, c], // CvcvCvccnoCvcc
            vec![C, v, c, v, C, v, c, c, C, v, c, c, n, o], // CvcvCvccCvccno
            vec![C, v, c, c, n, o, C, v, c, v, C, v, c, c], // CvccnoCvcvCvcc
            vec![C, v, c, c, C, v, c, v, n, o, C, v, c, c], // CvccCvcvnoCvcc
            vec![C, v, c, c, C, v, c, v, C, v, c, c, n, o], // CvccCvcvCvccno
        ],
        TemplateClasse::Medium => vec![
            vec![C, v, c, n, o, C, v, c], // CvcnoCvc
            vec![C, v, c, C, v, c, n, o], // CvcCvcno
        ],
        TemplateClasse::Short => vec![
            vec![C, v, c, n], // Cvcn
        ],
        TemplateClasse::Basic => vec![
            vec![a, a, a, n, a, a, a, n], // aaanaaan
            vec![a, a, n, n, a, a, a, n], // aannaaan
            vec![a, a, a, n, n, a, a, a], // aaannaaa
        ],
        TemplateClasse::PIN => vec![
            vec![n, n, n, n], // nnnn
        ],
        TemplateClasse::Name => vec![
            vec![c, v, c, c, v, c, v, c, v], // cvccvcvcv
        ],
        TemplateClasse::Phrase => vec![
            vec![c, v, c, c, S, c, v, c, S, c, v, c, c, v, c, v, S, c, v, c], // cvcc cvc cvccvcv cvc
            vec![c, v, c, S, c, v, c, c, v, c, v, c, v, S, c, v, c, v],       // cvc cvccvcvcv cvcv
            vec![c, v, S, c, v, c, c, v, S, c, v, c, S, c, v, c, v, c, c, v], // cv cvccv cvc cvcvccv
        ],
    }

    // ------- end template_classes -------
}
