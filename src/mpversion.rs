/**
 * `MPVersion`
 *
 * Represent the supported versions of the Spectre algorithm.
 *
 * Currently there is 4 supported version:
 *
 * `V0`, `V1`, `V2`, `V3`.
 */
#[derive(Clone, Copy)]
pub enum MPVersion {
    V0,
    V1,
    V2,
    V3,
}

/**
 * Return the version as an interger.
 *
 * Argument:
 *
 * - `version` - version of the Spectre algorithm wanted.
 */
pub fn mpw_version(version: MPVersion) -> usize {
    match version {
        MPVersion::V0 => 0,
        MPVersion::V1 => 1,
        MPVersion::V2 => 2,
        MPVersion::V3 => 3,
    }
}
