use std::convert::TryInto;

use hmac::{Hmac, Mac};
use scrypt::{scrypt, ScryptParams};
use sha2::Sha256;

#[allow(non_snake_case)]
pub fn mpw_scrypt(passwd: &[u8], salt: &[u8], N: u64, r: u32, p: u32, buf: &mut [u8]) {
    let log2_n: u8 = N
        .checked_ilog2()
        .expect("u32 number")
        .try_into()
        .expect("log fitting in u8");

    let params = ScryptParams::new(log2_n, r, p).expect("scrypt parameters");
    let res = scrypt(passwd, salt, &params, buf);

    match res {
        Ok(_) => (),
        Err(e) => panic!("scrypt error: {}", e),
    }
}

pub fn mpw_hmac_sha256(key: &[u8], input: &[u8]) -> [u8; 32] {
    type HmacSha256 = Hmac<Sha256>;

    let mut mac = HmacSha256::new_from_slice(key).expect("hmac can take key of any size");
    mac.update(input);

    let res = mac.finalize();

    res.into_bytes().into()
}
