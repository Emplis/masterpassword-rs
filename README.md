# masterpassword-rs

Implementation of the [Spectre](https://spectre.app/) algorithm in rust.

## How to use ?

Start by generating the master key by using the `calculate_key` function. Then you can generate password.

To do it generate the site key with the `site_key` function. The password can now be generated with `site_password`.

## Example

```rust
let masterkey: [u8; 64] = calculate_key("user", "password", MPVersion::V3);
let sitekey: [u8; 32] = site_key(masterkey, "example.com", 1, KeyScope::Authentification, MPVersion::V3);
let password: String = site_password(TemplateClasse::Long, sitekey, MPVersion::V3);

assert_eq!(password, "ZedaFaxcZaso9*".to_string());
```

## License

This work is licensed under [MIT License](LICENSE).
