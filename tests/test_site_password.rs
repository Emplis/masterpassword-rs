extern crate masterpassword_rs;

use masterpassword_rs::key_scope::KeyScope;
use masterpassword_rs::mpversion::MPVersion;
use masterpassword_rs::template_classe::TemplateClasse;

use masterpassword_rs::mpw::MPW;

#[test]
fn test_mpw_generate_authentification_password() {
    let new_mpw = MPW::new("user", "password", MPVersion::V3);
    let password =
        new_mpw.generate_authentification_password("example.com", 1, TemplateClasse::Long);

    assert_eq!(&password, "ZedaFaxcZaso9*");
}

#[test]
fn test_serialize() {
    let expected_serialized_data: [u8; 65] = [
        3, 200, 158, 204, 134, 255, 112, 160, 138, 97, 171, 25, 64, 145, 104, 253, 107, 79, 9, 202,
        35, 66, 201, 81, 197, 148, 39, 235, 88, 112, 146, 175, 242, 130, 42, 40, 74, 116, 234, 244,
        3, 125, 20, 147, 6, 239, 22, 56, 103, 102, 199, 32, 182, 121, 114, 44, 193, 252, 120, 55,
        144, 0, 180, 23, 238,
    ];

    let mpw = MPW::new("user", "password", MPVersion::V3);
    let serialized_data = mpw.serialize();

    assert_eq!(&serialized_data, &expected_serialized_data);
}

#[test]
fn test_deserialize() {
    let serialized_data: [u8; 65] = [
        3, 200, 158, 204, 134, 255, 112, 160, 138, 97, 171, 25, 64, 145, 104, 253, 107, 79, 9, 202,
        35, 66, 201, 81, 197, 148, 39, 235, 88, 112, 146, 175, 242, 130, 42, 40, 74, 116, 234, 244,
        3, 125, 20, 147, 6, 239, 22, 56, 103, 102, 199, 32, 182, 121, 114, 44, 193, 252, 120, 55,
        144, 0, 180, 23, 238,
    ];
    let expected_password = "ZedaFaxcZaso9*";

    let mpw = MPW::deserialize(serialized_data);
    let password = mpw.generate_authentification_password("example.com", 1, TemplateClasse::Long);

    assert_eq!(password, expected_password);
}
